Polyglot profile is a Drupal 7 installation profile with commonly used features for multilingual website
===========================================================================================================

Local setup
----------------


Rebuild
-----------
Run drush make without core from the profile folder

		$drush make --yes --working-copy --no-core --contrib-destination=. drupal-org.make

