<?php

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function polyglot_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implement hook_install_tasks().
 *
 * Add steps to the intall tasks to choose languages and provide translations.
 */
function polyglot_install_tasks($install_state) {
  return array(
    'polyglot_select_languages' => array(
      'display_name' => st('Choose languages'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'type' => 'form',
    ),
    'polyglot_import_translation' => array(
      'display_name' => st('Set up translations'),
      'display' => TRUE,
      'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
      'type' => 'batch',
    ),
  );
}

/**
 * Installation step callback.
 *
 * Provides a form for selecting multiple languages to install.
 *
 * @param $install_state
 * An array of information about the current installation state.
 */
function polyglot_select_languages($form, &$form_state, &$install_state) {
  include_once DRUPAL_ROOT . '/includes/iso.inc';
  $languages = _locale_get_predefined_list();
  $languages_processed = array();
  foreach ($languages as $langcode => $language) {
    $languages_processed[$langcode] = $language[0];
  }
  $form = array();
  $form['polyglot_languages'] = array(
    '#title' => t('Choose a language'),
    '#type' => 'checkboxes',
    '#required' => TRUE,
    '#options' => $languages_processed,
  );
  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Select'),
  );
  return $form;
}
/**
 * Task callback: Choose languages submit.
 *
 * Submit handler for the language selection form.
 * Adds the languages that were selected using the locale module.
 *
 */
function polyglot_select_languages_submit($form, &$form_state) {
  $selected_languages = $form_state['values']['polyglot_languages'];
  //List of all the languages that Drupal knows about.
  include_once DRUPAL_ROOT . '/includes/iso.inc';
  $all_languages = _locale_get_predefined_list();
  //List of languages that are already installed.
  include_once DRUPAL_ROOT . '/includes/locale.inc';
  $installed_languages = locale_language_list();
  foreach($selected_languages as $language) {
    if ($language && empty($installed_languages[$language])) {
      $name = isset($all_languages[$language][0]) ? $all_languages[$language][0] : $language;
      $native = isset($all_languages[$language][1]) ? $all_languages[$language][1] : $language;
      $direction = isset($all_languages[$language][2]) ? $all_languages[$language][2] : LANGUAGE_LTR;
      locale_add_language($language, $name, $native, $direction, '', $language, TRUE, FALSE);
    }
  }
}
/**
 * Installation step callback.
 *
 * Imports translations for these from localize.drupal.org using l10n_update.
 *
 * @param $install_state
 * An array of information about the current installation state.
 */
function polyglot_import_translation(&$install_state) {
// Build batch with l10n_update module.
  $history = l10n_update_get_history();
  module_load_include('check.inc', 'l10n_update');
  $available = l10n_update_available_releases();
  $updates = l10n_update_build_updates($history, $available);
  module_load_include('batch.inc', 'l10n_update');
  $updates = _l10n_update_prepare_updates($updates, NULL, array());
  $batch = l10n_update_batch_multiple($updates, LOCALE_IMPORT_KEEP);
  return $batch;
}