; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
  
; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.
  
api = 2
  
; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.
  
; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[drupal][version] = 7


; Modules
; --------
defaults[projects][subdir] = "contrib"


projects[admin_language] = 1.0-beta1
projects[admin_menu] = 3.0-rc5
projects[advagg] = 2.7
projects[addthis][version] = 4.0-alpha4
projects[backup_migrate] = 3.0
projects[better_formats][version] = 1.0-beta1
projects[chosen][version] = 2.0-beta4
projects[ctools] = 1.5
projects[date] = 2.8

projects[devel][version] = 1.5
projects[devel][subdir] = "develop"

projects[diff] = 3.2
projects[ds] = 2.7
projects[entity] = 1.5
projects[entity_translation] = 1.0-beta3
projects[entityreference][version] = 1.1
projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"
projects[entityreference][patch][1836106] = https://www.drupal.org/files/issues/entityreference-1836106-20.patch

projects[extlink] = 1.18
projects[features] = 2.3
projects[field_group] = 1.4
projects[google_analytics] = 2.1
projects[i18n] = 1.11
projects[jquery_update] = 2.4
projects[l10n_update] = 1.1
projects[libraries] = 2.2
projects[link] = 1.3
projects[metatag] = 1.4
projects[module_filter][version] = 2.0-alpha2
projects[opengraph_meta] = 1.3
projects[pathauto] = 1.2
projects[path_breadcrumbs] = 3.1
projects[shield] = 1.2
projects[special_menu_items] = 2.0
projects[strongarm] = 2.0
projects[smart_trim] = 1.4
projects[title] = 1.0-alpha7
projects[token] = 1.5
projects[variable] = 2.5
projects[views][version] = 3.8
projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views][patch][1909136] = https://www.drupal.org/files/views-7.x-3.x-combine-filter-1909136-6.patch


projects[wysiwyg][version] = 2.2
projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"

projects[wysiwyg_button_order][version] = 1.0-rc1
projects[wysiwyg_button_order][type] = "module"
projects[wysiwyg_button_order][subdir] = "contrib"

projects[wysiwyg_filter][version] = 1.6-rc2
projects[wysiwyg_filter][type] = "module"
projects[wysiwyg_filter][subdir] = "contrib"

projects[wysiwyg_linebreaks][version] = 1.6
projects[wysiwyg_linebreaks][type] = "module"
projects[wysiwyg_linebreaks][subdir] = "contrib"

projects[xmlsitemap] = 2.1

; Themes
; --------

; Libraries
; ---------

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6/ckeditor_3.6.6.tar.gz"


libraries[chosen][download][type] = "get"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/v1.1.0/chosen_v1.1.0.zip"